$(document).ready(function(){
    /*
    $(window).scroll(function(){
        $(".full").each(function(){
            var sectionID = $(this).attr("id");
            var sectionHeight = $(this).outerHeight();
            var x = $(this).offset().top;
            if($(window).scrollTop() > x && $(window).scrollTop < x + sectionHeight)
            {
                $(".menu-item-content").attr("id" , "#"+sectionID).addClass("active-tab");
            }
            else
            {
                $(".menu-item-content").attr("id" , "#"+sectionID).removeClass("active-tab");
            }
        });
    });
    */

    $(".menu-item-content").click(function(e){
        $('.menu-item-content').each(function() {
            $(this).removeClass("active-tab");
        });
        $(this).addClass("active-tab");
    });

    $("#send-msg").click(function(){
        if($("#cname").val().trim() != "" && $("#cemail").val().trim() != "" && $("#cmsg").val().trim() != "")
        {
            var cname = $("#cname").val().trim();
            var cemail = $("#cemail").val().trim();
            var cmsg = $("#cmsg").val().trim();
            $.ajax({
                url: "",
                type: "POST",
                data: {
                    cname:cname,
                    cemail:cemail,
                    cmsg:cmsg
                },
                success: function(data){
                    if(data == 1)
                        $(".contact-form").empty().append("<div class='contact-success'><i class='icon-ok'></i><span>تم الارسال بنجاح</span></div>");
                    else
                        $(".contact-err").fadeIn(500).delay(5000).fadeOut(500);
                }
            });
        }
        else
        {
            $(".contact-err").fadeIn(500).delay(5000).fadeOut(500);
        }
    });

    $(".tab").click(function(){
        var ele = $("header i", this);
        $(".answer", this).slideToggle("slow",function(){
            ele.toggleClass("icon-minus");
        });
    });
});